## MOVIES

Movies é uma aplicação do estilo catalogo, que permite além de ver informações sobre os principais e mais populares filmes do momento ele também permite pesquisar os seus filmes e adicioná-los aos favoritos.


## HOME
    Lista um filme principal como destaque
    Quatros listas sendo elas:
        * Melhores notas de todos os tempos
        * Mais populares do momento
        * Atualmente no cinema
        * Os próximos a serem lançados
        * Utilize preferencialmente no android a aplicação pode se comportar de maneira indesejadas nos iOS assim com ter sua interface bagunçada, já que o mesmo não foi testado no iOS.

## SEARCH
    Pesquisa por filmes, nome ou gêneros

## FAVORITES
    Aqui você encontra sua lista de filmes

## MOVIE PAGES
    Nesta página você encontra informações mais detalhadas sobre cada filme

## PROBLEMAS
    Dependendo do ambiente que você escolher testar, pelo Expo ou baixando o apk pode apresentar lentidão na home e páginas vinculadas à elas, causado por um número alto de requisições ao iniciar a aplicação.
    Animação ao adicionar/remover filme dos favoritos um pouco lento
    (Não é problema) Como a api me fornece alguns filmes sem overview em pt-BR optei por utilizar tudo em inglês
    Pode acontecer também da aplicação não apresentar um filme no card principal da Home, isso acontece porque alguns ids não estão disponíveis. Caso isso acontecer fecha a aplicação(limpe da multitarefa) e abra novamente.

## ONDE TESTAR
   [No repositório do Expo usando o cliente App para Android](https://expo.io/@tassiorocha/movies)
   ou [baixando o apk](https://exp-shell-app-assets.s3-us-west-1.amazonaws.com/android%2F%40tassiorocha%2Fmovies-d6acb96c-8afb-11e8-9192-0a580a781f31-signed.apk).
   
   Utilize preferencialmente no android a aplicação pode se comportar de maneiras indesejadas nos iOS assim com ter sua interface bagunçada, já que o mesmo não foi testado no iOS.


## INFORMAÇÕES FINAIS
    Versão 0.1.0
    Pretendo manter está aplicação sempre atualizada
    Haverá sempre que possível a manutenção do código
