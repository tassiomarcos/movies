const INITIAL_STATE = {
    movies: null,
    movie: null,
    similar_movies: null,
    card_movie: null,
    iconState: false,
    favorites: null
}


export default (state = INITIAL_STATE, action) => {

    switch(action.type){
        case 'TOP_RATED_MOVIES': 
            return { ...state, movies: action.payload }
        case 'MOVIE_BY_ID':
            return { ...state, movie: action.payload }
        case 'SIMILAR_MOVIES':
            return { ...state, similar_movies: action.payload }
        case 'CARD_MOVIE':
            return { ...state, card_movie: action.payload }
        case 'PLAYING_NOW_MOVIES':
            return { ...state, playing_movie: action.payload }
        case 'UPCOMING_MOVIES':
            return { ...state, upcoming_movie: action.payload }            
        case 'POPULAR_MOVIES':
            return { ...state, popular_movie: action.payload }
        case 'SEARCH_MOVIES':
            return { ...state, search_movies: action.payload }
        case 'FAVORITE_STATE':
            return { ...state, iconState: action.payload }
        case 'FAVORITES_MOVIE':
            return { ...state, favorites: action.payload }
        default: 
            return state;
    }
}