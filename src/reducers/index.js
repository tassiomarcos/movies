import { combineReducers } from 'redux';

import MovieReducer from './movie';
import NavigationReducer from './navigation';

export default combineReducers({
    movies: MovieReducer,
    navigation: NavigationReducer
})

