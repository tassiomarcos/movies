import React from 'react';
import { View, Text, TouchableHighlight, Image, FlatList, Dimensions } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getSimilarMovies } from '../actions';

const imageUrl = value => `https://image.tmdb.org/t/p/w500/${value}`
const numColumns = 3;
const size = Dimensions.get('window').width/numColumns;



const MovieSuggestion = ({ movies, func }) =>  {

    if(!movies || movies.length === 0){
        return ( <Text style={styles.text}> Don't have movies in the moment </Text> );
    }

    return (
        <FlatList
            data={movies}
            renderItem={ ({item}) => (
                            <TouchableHighlight 
                                style={styles.itemContainer} 
                                onPress={(e) => { e.preventDefault(); func(item.id) }}>

                                    <Image style={styles.item} 
                                            source={{ 
                                                uri: imageUrl(item.poster_path),
                                                cache: 'force-cache' }} />

                            </TouchableHighlight>
                )}
            keyExtractor={ item => item.id }
            initialNumToRender={9}
            numColumns={numColumns} 
        /> 
    );
}
    

const styles = {
    itemContainer: {
        width: size,
        height: 200,
        paddingRight: 5,
        paddingTop: 3
    },
    item: {
        flex: 1,
        margin: 3,
        backgroundColor: 'lightblue',
    },
    text: {
    color: 'white',
    fontSize: 20,
    marginTop: 10
    },
    showBackImage:{
        height: 150,
        width: '33%',
        margin: 10,
        marginLeft: 5,
        marginRight: 5
    }
}


const mapStateToProps = ({ movies }) => {
    return { similar: movies.similar_movies  };
}

const mapToDispatch = dispatch => {
    return bindActionCreators({ getSimilarMovies }, dispatch);
}


export default connect(mapStateToProps, mapToDispatch)(MovieSuggestion);