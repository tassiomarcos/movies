import React from 'react';
import { View, 
         Text, 
         Image, 
         TouchableOpacity, 
         StyleSheet } from 'react-native';



const buttonImage = ({ pathImage, text, func, style }) => {
    
    return (
        <TouchableOpacity onPress={(e) =>{ e.preventDefault; func() }}>
            <View style={[styles.container]}>
                <Image style={[styles.image, style]} source={ pathImage } />
                <Text style={styles.text}> { text }</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        zIndex: 2,
        alignItems: 'center',
        padding: 15,
        height: 50
    },
    image: {
        width: 20,
        height: 20
    },
    text: {
        color: 'white',
        fontSize: 12,
        fontWeight: 'normal',
    }
})



export default buttonImage;





