import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';
import { verticalScale } from '../utils/dimensions';

const Icon = ({ favorite, onPress, id, poster_path, style }) => (
    <TouchableOpacity onPress={() => onPress(id, poster_path)}>   
        {
            !favorite
            ? <Image style={[style, styles.img]} source={require('../assets/plus.png')} />
            : <Image style={[styles.img, style]} source={require('../assets/tick.png')} />
        }
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    img: {
        height: verticalScale(15),
        width: verticalScale(15)
    }
})

export { Icon };