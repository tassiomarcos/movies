import React from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { } from 'redux';
import Loading from './loading';

const imageUrl = value => `https://image.tmdb.org/t/p/w500/${value}`

const movieList = ({ movies, title, func }) => {

    if(!movies) { 
        return (
            <Loading /> 
        );
    }
    else {
        return(
            <View>
                <Text style={styles.text}> { title } </Text>
                <ScrollView horizontal={true}>
                    { movies.map(movie => 
                        <TouchableOpacity activeOpacity={1} 
                                onPress={e => {e.preventDefault(); func(movie.id) }}
                                key={movie.id} >
                        <Image
                                source={{ uri: (imageUrl(movie.poster_path)) }} 
                                style={styles.showBackImage}>
                        </Image>
                        </TouchableOpacity> )}
                </ScrollView>
            </View>
        );
    }
} 

const styles = StyleSheet.create({
    text: {
        color: 'brown',
        fontSize: 20,
        marginTop: 10
    },
    showBackImage: {
        height: 150,
        width: 100,
        margin: 10,
        marginLeft: 5,
        marginRight: 5
    }
});

export default movieList;