import React from 'react';
import { colors } from '../utils/colors';
import { View, ActivityIndicator } from  'react-native';


const loading = () => (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" color={colors.primary}/>
    </View>
);

export default loading;