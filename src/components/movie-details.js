import React from 'react';
import { View, Text , ScrollView, Animated } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Button from '../components/image-button';
import MoviePageSug from '../components/movie-grid';
import Description from '../components/description';

import Loading from '../components/loading';
import { getMovieById, getSimilarMovies } from '../actions';
import { iconVerify, addRemoveFavorite } from '../actions/favorites';



const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = 60;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

class MoveDetail extends React.Component {
    
    constructor(props){
        super(props);
        const id = this.props.navigation.getParam('id');
        this.state = {
            movie_id: id,
            scrollY: new Animated.Value(0),
            favIcon: false
        }
    }
    
    async componentDidMount(){
        await this.props.getMovieById(this.state.movie_id);
        await this.props.getSimilarMovies(this.state.movie_id);
        await this.props.iconVerify(this.state.movie_id);
        this.setState({ favIcon: this.props.icon })
    }

    async componentWillUpdate(){
        await this.props.iconVerify(this.state.movie_id);
        this.setState({ favIcon: this.props.icon })
    }

    _icon(){
        if(this.state.favIcon){
            return require('../assets/tick.png')
        } else {
            return require('../assets/plus.png')
        }
    }

    async _addRemove(id, poster_path){
        await this.props.addRemoveFavorite(id, poster_path);
    }

    _backTo(){
        this.props.navigation.goBack();
    }

    async _changeMovie(id){
        await this.props.getMovieById(id);
        await this.props.getSimilarMovies(id);
        await this.props.iconVerify(this.state.movie_id);
        this.setState({ favIcon: this.props.icon })
    }

    render(){

        const top = this.state.scrollY.interpolate({
            inputRange:  [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [30,30,30],
            extrapolate: 'clamp',
        });  

        const backgroundColor = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: ['rgba(25,25,25,0)', 'rgba(25,25,25,0.5)', 'rgba(25,25,25,1)'],
            extrapolate: 'clamp'
        })
  
   
        const { movie } = this.props.movie;
        const { similar } = this.props;

        if(!movie){
            return (
                <Loading />
            );
        }

        else {
            return(
                <View style={{ backgroundColor: '#141414', flex: 1 }}>
                    <Animated.View style={[styles.header, 
                        {  backgroundColor: backgroundColor }]} >
                        <Button pathImage={require('../assets/arrow.png')}
                            func={() => this._backTo()} />
                    </Animated.View>
                    <ScrollView 
                        scrollEventThrottle={16}  
                        onScroll={Animated.event([{
                            nativeEvent: {contentOffset: {y: this.state.scrollY}}
                        }])}>
                        
                        <Description    backdrop_path={movie.backdrop_path} 
                                        runtime={movie.runtime} 
                                        title={movie.title} 
                                        release_date={movie.release_date} 
                                        vote_average={movie.vote_average}/>
                                        
                        <Text style={styles.overview}>{ movie.overview }</Text>
                        
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Button pathImage={this._icon()} 
                                    text={'Favorites'} 
                                    func={() => this._addRemove(movie.id, movie.poster_path) }/>
                            <Button pathImage={require('../assets/share.png')} 
                                    text={'Share'} 
                                    func={() => alert('compartilhado')}/>
                        </View>

                        <Text style={styles.titleRecomendations}> 
                            SIMILAR OPTIONS 
                        </Text>
                        <MoviePageSug movies={similar} 
                                      func={(e) => this._changeMovie(e)} />
                    </ScrollView>
                </View>
            )
        }
    }
}

const styles = {
    overview: {
        color: 'white',
        fontSize: 12,
        padding: 5
    },
    titleRecomendations: {
        color: 'white',
        padding: 5
    },
    header: { 
        height: 75,
        alignItems: 'flex-start', 
        justifyContent: 'flex-end', 
        backgroundColor: 'yellow',
        zIndex: 1, 
        width: '100%',
        position: 'absolute',
        top: 0
    }
 
}

const mapStateToProps = ({ movies }) => ({
    movie: movies, 
    similar: movies.similar_movies, 
    icon: movies.iconState 
})

const mapToDispatch = dispatch => ({
    addRemoveFavorite: () => dispatch(addRemoveFavorite()),
    getMovieById: () => dispatch(getMovieById()),
    getSimilarMovies: () => dispatch(), 
    iconVerify: () => dispatch(),
})

export default connect(mapStateToProps, mapToDispatch)(MoveDetail);