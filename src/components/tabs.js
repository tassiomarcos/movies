import React from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, Text, TouchableWithoutFeedback } from 'react-native';
import { verticalScale } from '../utils/dimensions';
import { colors } from '../utils/colors';
import { navigate } from '../actions/navigation';

const getSelected = (selected, index) => {
    return selected === index
            ? { borderBottomWidth: 3, borderColor: colors.brown }
            : { borderBottomWidth: 3, borderColor: colors.primary }
}

class Tabs extends React.Component {

    handleSelect = async (selected) => {
        const { navigate } = this.props;
        await navigate(selected);
    }

    render(){
        const { page } = this.props;

        return(
                <View style={styles.container}>
                    <TouchableWithoutFeedback  onPress={() => this.handleSelect(1)}>
                        <View style={styles.btn}>
                            <Text style={[styles.text, getSelected(page, 1)]}>Home</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => this.handleSelect(2)}>
                        <View style={styles.btn}>
                            <Text style={[styles.text, getSelected(page, 2)]}>Favorites</Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => this.handleSelect(3)}>
                        <View style={styles.btn}>
                            <Text style={[styles.text, getSelected(page, 3)]}>Search</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: verticalScale(55),
        flexDirection: 'row',
        backgroundColor: colors.primary,
        alignItems: 'center',
        justifyContent: 'space-around',
        elevation: 10
    },
    btn: {
        height: '100%',
        width: '33%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: colors.brown,
        fontSize: verticalScale(12),
        fontWeight: "bold"
    }
    
})

const mapStateToProps = ({ navigation }) => ({
    page: navigation.page
})

const mapDispatchToProps = dispatch => ({
    navigate: selected => dispatch(navigate(selected)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Tabs);