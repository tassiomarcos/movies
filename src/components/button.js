import React from 'react';
import { Text, TouchableHighlight } from 'react-native';

const button = ({ text, func, styles }) => {
    return(
        <TouchableHighlight style={styles} 
                            onPress={() => { func() }}>

            <Text style={{color: 'black'}}> 
                { text } 
            </Text>
            
        </TouchableHighlight>
    );
}

export default button;