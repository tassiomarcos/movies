import React from 'react';
import { Image, StyleSheet, TouchableHighlight, Text, View } from 'react-native';
import { Icon } from './favoriteIcon';
import { verticalScale } from '../utils/dimensions';
import { colors } from '../utils/colors';
import { year, imgUrl, formatNumber } from '../utils/functions';

const Banner = ({ movie, onPress, icon, icon_state }) => {
    icon(movie.id);

    return <View>
        <TouchableHighlight onPress={() => {}}>
            <View style={styles.view}>
                <Image style={styles.image} 
                    source={{ uri: (imgUrl(movie.backdrop_path))}} 
                />
            </View>
        </TouchableHighlight>
        <View style={styles.contianerBtns}>
            <Text style={[styles.text, styles.title]}>{movie.title}</Text>
            <Text style={styles.text}>{year(movie.release_date)}</Text>
            <Text style={[styles.text, styles.average]}>{formatNumber(movie.vote_average)}</Text>
            <Icon  favorite={icon_state} style={styles.icon} onPress={onPress} poster_path={movie.poster_path} id={movie.id} />
            <Image style={styles.icon} source={require('../assets/info.png')}/>
        </View>
    </View>
   
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
    },
    title: {
        width: '100%',
    },
    text: {
        color: colors.snow,
        padding: verticalScale(5),
        fontSize: verticalScale(10),
        textAlign: 'center',
    },
    icon: {
        height: verticalScale(15),
        width: verticalScale(15),
        marginLeft: verticalScale(5),
        marginRight: verticalScale(5),
        padding: 5,
    },
    average: {
        color: colors.average,
    },

    contianerBtns: {
      width: '100%',
      height: verticalScale(50),
      backgroundColor: 'rgba(0,0,0,0.6)',
      top: '80%',
      position: 'absolute',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      flexWrap: 'wrap'
    },
    image: {
        width: '100%',
        height: verticalScale(244.5),
    }
}) 



export default Banner;