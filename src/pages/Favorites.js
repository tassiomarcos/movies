import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { colors } from '../utils/colors';

import MovieGrid from '../components/movie-grid';

class Favorites extends React.Component {

    _fowardTo(id){
        this.state.navigation.navigate('MoviePage', {id});
    }


    render(){
  
        const { movies } = this.props;

        return(
            <View  style={styles.container}>
                { !movies.length 
                ? <Text style={styles.text}> You don't have saved favorites movies </Text>
                : <MovieGrid movies={movies} func={(e) => this._fowardTo(e)}/>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.snow,
        padding: 5
    },

    text: {
        color: colors.brown,
        fontSize: 20,
        marginTop: 30
    }
})

const mapStateToProps = ({ movies }) => ({
  movies: movies.favorites || [],
})

export default connect(mapStateToProps)(Favorites)