import React from 'react';
import { View, StyleSheet, StatusBar } from 'react-native';
import { connect } from 'react-redux';

import { addRemoveFavorite, iconVerify } from '../actions/favorites';

import Tabs from '../components/tabs';
import Top from './Top';
import Favorites from './Favorites';

import { colors } from '../utils/colors';
import Search from './Search';

class Home extends React.Component {

    handleAddRemoveFavorites = async (id, poster) => {
        const { addRemoveFavorite } = this.props;
        await addRemoveFavorite(id, poster);
    }

    handleIconVerify = (id) => {
        const { iconVerify } = this.props;
        iconVerify(id)
    }

    render(){
        const { page } = this.props;

        
        return(
            <View style={styles.container}>
                <StatusBar backgroundColor={colors.primary} />
                <Tabs />
                { page === 1 ? <Top  func_icon={this.handleIconVerify} func_favorite={this.handleAddRemoveFavorites} /> : null }
                { page === 2 ? <Favorites /> : null }
                { page === 3 ? <Search /> : null }
                { page === 4 ? <Top /> : null }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.snow
    }
})

const mapStateToProps = ({ navigation }) => ({
    page: navigation.page
})

const mapDispatchToProps = dispatch => ({
    addRemoveFavorite: (id, poster_path) => dispatch(addRemoveFavorite(id, poster_path)),
    iconVerify: id => dispatch(iconVerify(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);
