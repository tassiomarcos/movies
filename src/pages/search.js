import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { colors } from '../utils/colors';
import { verticalScale } from '../utils/dimensions';


import { searchMovies } from '../actions';
import MovieGrid from '../components/movie-grid';

import Button from '../components/button';

class Search extends React.Component {
    
    constructor(props){
        super(props);
    }

     _catchString(e){
        this.setState({ query: e })    
    }

    _fowardTo(id){
        this.props.navigation.navigate('MoviePage', { id });
    }

    async _searchMovie(){
        await this.props.searchMovies(this.state.query); 
    }

    render(){

        const { movies } = this.props;
        return(
                <View style={ styles.container }>
                    <View style={ styles.searchBar }>
                        <TextInput style={ styles.input } 
                                    onChangeText={ (e) => this._catchString(e) } 
                                    underlineColorAndroid='transparent'
                                    placeholder="Search for a name of movie" />
                        {/* <Button styles={styles.button} 
                                textColor={styles.textButton} 
                                text='Search' func={ () => this._searchMovie() } /> */}
                    </View>
                    { movies 
                        ? <MovieGrid movies={ movies } func={ e => this._fowardTo(e) } />
                        : null
                    }
                </View>
            )
        }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor: colors.snow,
        flex: 1,
        paddingTop: verticalScale(10)
    },
    searchBar:{
        backgroundColor: "transparent",
        justifyContent: 'center',
        flexDirection: 'row',
        padding: 10
    },
    input: {
        height: 40,
        width: '100%',
        margin: 10,
        padding: 10,
        color: '#fff',
        borderWidth: 2,
        borderRadius: verticalScale(15),
        borderColor: colors.brown
    },
    button: {
        backgroundColor: 'red',
        height: 30,
        width: '30%',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textButton: {
        color: 'grey'
    }

})

const mapStateToProps = ({ movies }) => {
    return { movies: movies.search_movies }
}

const mapToDispatch = dispatch => {
    return bindActionCreators({ searchMovies }, dispatch);
}

export default connect(mapStateToProps, mapToDispatch)(Search);