import React from 'react';
import { StyleSheet, ScrollView, View, ActivityIndicator} from 'react-native';
import { connect } from 'react-redux';
import Banner from '../components/banner';
import Loading from '../components/loading';
import MovieList from '../components/movies-list';

class Top extends React.Component {
    
    render(){
        const { top, upcoming_movie, playing_movie, popular_movie  } = this.props;

        if(!top || !upcoming_movie || !playing_movie || !popular_movie) {
            return <Loading />
        }
        return(
            <ScrollView style={styles.container}>
                <Banner
                    icon_state={this.props.iconState}
                    icon={this.props.func_icon}
                    onPress={this.props.func_favorite}
                    movie={top[0]} 
                    source={{ uri: (`https://image.tmdb.org/t/p/w500/${top[0].backdrop_path}`) }} 
                />
                <MovieList title="Top Rated" movies={top} />
                <MovieList title="Upcoming" movies={upcoming_movie} />
                <MovieList title="Playing Now" movies={playing_movie} />
                <MovieList title="Popular" movies={popular_movie} />
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})

const mapStateToProps = ({ movies }) => ({
    upcoming_movie: movies.upcoming_movie,
    playing_movie: movies.playing_movie,
    popular_movie: movies.popular_movie,
    top: movies.movies,
    icon: movies.iconState
})

export default connect(mapStateToProps)(Top);
