import React from 'react';
import { connect } from 'react-redux';

import { View, Image, ActivityIndicator, StatusBar } from 'react-native';
import { verticalScale } from '../utils/dimensions';

import Home from './Home';
import { getPlayingMovies, getTopRatedtMovies, getUpComingMovies, getPopularMovies, } from '../actions';
import { getFavoritesMovie, createTable } from '../actions/favorites';

class Init extends React.Component {
    
    state = {
        initialized: false
    }

    async componentDidMount() {
        const { getMovies, getPlayingMovies, getPopularMovies, getUpComingMovies, getFavoritesMovie } = this.props;
        await Promise.all([
            getMovies(),
            getPlayingMovies(),
            getPopularMovies(),
            getUpComingMovies(),
            createTable(),
            getFavoritesMovie()
        ])
            .then(() => this.setState({ initialized: true }))
            .catch(err => console.log(err))

    }

    render(){

        if(this.state.initialized) return <Home />

        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <StatusBar backgroundColor="white" />
                <Image source={require('../assets/icon.png')} 
                    style={{ 
                        width: verticalScale(50), 
                        marginBottom: verticalScale(15), 
                        height: verticalScale(50) }} />
                <ActivityIndicator size="large" color="red" />
            </View>
        )
    }
}



const mapDispatchToProps = dispatch => ({
    getMovies: () => dispatch(getTopRatedtMovies()),
    getPlayingMovies: () => dispatch(getPlayingMovies()),
    getPopularMovies: () => dispatch(getPopularMovies()),
    getUpComingMovies: () => dispatch(getUpComingMovies()),
    getFavoritesMovie: () => dispatch(getFavoritesMovie())
});


export default connect(null, mapDispatchToProps)(Init);
