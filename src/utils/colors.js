const colors = {
    primary: '#ff6347',
    average: '#adff2f',
    snow: '#ffffff',
    brown: '#453823'
}

export { colors }