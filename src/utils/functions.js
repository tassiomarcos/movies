const  year = (value) => {
    var date = new Date(value);
    return date.getFullYear();
}
const imgUrl = value => `https://image.tmdb.org/t/p/w500/${value}`
const formatNumber = (value) => {
    if(value === 0){
        return 'Novo'
    } else {
        return `${(value * 10).toFixed(0)}% relevante` ;
    }
}

export { formatNumber, imgUrl, year }