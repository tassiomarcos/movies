import { Dimensions } from 'react-native';


const { width, height } = Dimensions.get('window');

const base_unit = 512;

const verticalScale = size => height / base_unit * size;


export {
    verticalScale
}