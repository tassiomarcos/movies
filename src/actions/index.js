import axios from 'axios';

const URL = 'https://api.themoviedb.org/3/movie';
const key = '53312c5af553e6206f705572bd518711';

let data;

export const getTopRatedtMovies = async () => { 
      
    await axios.get(`${URL}/top_rated?api_key=${key}&page=1`)
                .then(e => data = e.data.results)
    return {
        type: 'TOP_RATED_MOVIES',
        payload: data
    }
}

export const getPlayingMovies = async () => {
    await axios.get(`${URL}/now_playing?api_key=${key}&page=1`)
                .then(e => data = e.data.results)
    return {
        type: 'PLAYING_NOW_MOVIES',
        payload: data
    }
}

export const getUpComingMovies = async () => {
    await axios.get(`${URL}/upcoming?api_key=${key}&page=1`)
                .then(e => data = e.data.results)
    return {
        type: 'UPCOMING_MOVIES',
        payload: data
    }
}

export const getPopularMovies = async () => {
    await axios.get(`${URL}/popular?api_key=${key}&language=en-US`)
                .then(e => data = e.data.results)
    return {
        type: 'POPULAR_MOVIES',
        payload: data
    }
}

export const getMovieById = async (id) => {

    await axios.get(`${URL}/${id}?api_key=${key}`)
                .then(e => data = e.data);
    return {
        payload: data,
        type: 'MOVIE_BY_ID'
    }

}

export const getMovieFromCard = async (id) => {

    await axios.get(`${URL}/${id}?api_key=${key}`)
            .then(e => data = e.data);
    return {
        payload: data,
        type: 'CARD_MOVIE'
    }

}

export const getSimilarMovies = async (id) => {

    await axios.get(`${URL}/${id}/similar?api_key=${key}&page=1`)
                .then(e => data = e.data.results);
    return {
        type: 'SIMILAR_MOVIES',
        payload: data
    }
}

export const searchMovies = async (query) => {

    await axios.get(`https://api.themoviedb.org/3/search/movie?api_key=${key}&language=en-US&query=${query}&page=1&include_adult=true`)
                .then(e => data= e.data.results);
    return {
        type: 'SEARCH_MOVIES',
        payload: data
    }

}
