export const NAVIGATION = 'NAVIGATION';


const navigation = selected => ({
    type: NAVIGATION,
    payload: selected
});


export const navigate = selected => {
    return {
        type: NAVIGATION,
        payload: selected
    }
}