import  { SQLite }from 'expo';

const db = SQLite.openDatabase('favorites.db');
let data = [];

// Função para resolver promessa do SQLite
const executeSql = (sql, params = []) => {
    return new Promise((resolve, reject) => db.transaction(tx => {
      tx.executeSql(sql, params, (_, { rows }) => resolve(rows._array), reject)
    }))
}

export const createTable = () => {
    db.transaction( tx =>
        tx.executeSql('create table if not exists favorite (id integer, poster_path text);')
    )
}

const search = (value) => {
    db.transaction(tx => {
        tx.executeSql(`select * from favorite where id = ${value}`, 
                        [], 
                        (_, { rows: _array }) => { data =_array }, null);
        });
    return data.length === 0 ? false : true;
}

export const addRemoveFavorite = async (id, poster_path) => {
    
    const check = search(id);

    if(check){
        await executeSql('delete from favorite where id = ?', [id])
        return{
                type: 'FAVORITE_STATE',
                payload: false
            }
    } 
    else {
       executeSql('insert into favorite (id, poster_path) values(?,?)', [id, poster_path])
        .then(res => console.log('res:', res))
            .catch(err => console.log('err:',err))
        return{
            type: 'FAVORITE_STATE',
            payload: true
        }
    } 
}

export const iconVerify = async (id) => {
    
    await executeSql(`select * from favorite where id = ${id}`, []).then( e => data = e);

    if(data.length === 0){
        return {
            payload: false,
            type: 'FAVORITE_STATE'
        }
    }
    else {
        return {
            payload: true,
            type: 'FAVORITE_STATE'
        }
    }

}

export const getFavoritesMovie = async () => {

   await executeSql('select * from favorite', []).then(movies => data = movies)

   return {
        type: 'FAVORITES_MOVIE',
        payload: data
    }

}