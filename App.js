import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxPromise from 'redux-promise';

import reducers from './src/reducers';
import Init from './src/pages/Init';



const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);
const store = createStoreWithMiddleware(reducers);


export default class App extends React.Component {
  render(){
    return(
      <Provider store={store}>
        <Init />
      </Provider>
    )
  }
}